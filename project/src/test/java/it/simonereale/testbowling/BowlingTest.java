package it.simonereale.testbowling;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import it.simonereale.bowling.controller.BowlingGame;
import it.simonereale.bowling.exception.GameException;

public class BowlingTest {

	private BowlingGame game;

	@Before
	public void setUp() {
		game = new BowlingGame();
	}

	// This test verify the CGM case shared
	@Test
	public void testCGM() {

		game.roll(1);
		game.roll(4);
		game.roll(4);
		game.roll(5);
		game.roll(6);
		game.roll(4);
		game.roll(5);
		game.roll(5);
		game.roll(10);
		game.roll(0);
		game.roll(1);
		game.roll(7);
		game.roll(3);
		game.roll(6);
		game.roll(4);
		game.roll(10);
		game.roll(2);
		game.roll(8);
		game.roll(6);

		int score = game.getScore();
		Assert.assertEquals(133, score);

		System.out.println(game);

	}

	@Test
	public void testSpare() {

		game.roll(4);
		game.roll(6);

		int score = game.getScore();
		Assert.assertEquals(10, score);

		game.roll(5);

		score = game.getScore();
		Assert.assertEquals(20, score);

	}

	@Test
	public void testStrike() {

		game.roll(6);
		game.roll(3);
		game.roll(10); // 19
		game.roll(3);
		game.roll(3);
		// 19 + 6 = 25 (for strike bonus)
		// 25 + 3 + 3 => 31

		int score = game.getScore();

		Assert.assertEquals(31, score);

	}

	@Test
	public void testStrikeOnFirstAttempt() {

		game.roll(10);

		int score = game.getScore();
		Assert.assertEquals(10, score);

		game.roll(5);
		game.roll(4);

		// 19 + 9 = 28

		score = game.getScore();
		Assert.assertEquals(28, score);

	}

	@Test
	public void testStrikeEveryRoll() {

		for (int i = 0; i < 12; i++) {
			game.roll(10);
		}

		int score = game.getScore();
		Assert.assertEquals(300, score); // max points

		System.out.println(game);

	}

	@Test(expected = GameException.class)
	public void testExceptionPins() {
		game.roll(10);
		game.roll(11); // Exception

	}

	@Test(expected = GameException.class)
	public void testExceptionRolls() {
		for (int i = 0; i < 22; i++) {
			game.roll(3);
		}

	}

}