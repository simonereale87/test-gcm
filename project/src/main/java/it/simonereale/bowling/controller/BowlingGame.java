package it.simonereale.bowling.controller;

import java.util.ArrayList;
import java.util.List;

import it.simonereale.bowling.exception.GameException;
import it.simonereale.bowling.model.Frame;

public class BowlingGame {

	private static final int MAX_ROLLS = 21;
	private static final int MAX_FRAMES = 10;
	private static final int MAX_PINS = 10;

	private int[] rolls = new int[MAX_ROLLS];
	private int currentRoll = 0;
	private List<Frame> frames;

	// init frames list
	{
		frames = new ArrayList<>();
		for (int i = 0; i < MAX_FRAMES; i++) {
			frames.add(new Frame());
		}
	}

	public void roll(int pins) {
		if (pins > MAX_PINS) {
			throw new GameException(String.format("Max pins exceeded: founded %s, permitted %s", pins, MAX_PINS));
		}
		if (currentRoll >= MAX_ROLLS) {
			throw new GameException(String.format("Max roll exceeded: permitted %s", currentRoll, MAX_ROLLS));
		}
		rolls[currentRoll++] = pins;
	}

	// The frames are 10 but the max roll is (10*2) + 1 bonus
	// Iterate the rolls and move with index
	public int getScore() {
		int score = 0;
		int frameIndex = 0;

		for (int frame = 0; frame < MAX_FRAMES; frame++) {
			if (isStrike(frameIndex)) { // if is strike sum two position forward
				score += MAX_PINS + getStrikeBonus(frameIndex);
				frames.get(frame).setStrike(true);
				frameIndex++;
			} else if (isSpare(frameIndex)) { // if is spare sum the next roll
				score += MAX_PINS + getSpareBonus(frameIndex);
				frames.get(frame).setSpare(true);
				frames.get(frame).pushAttempt(rolls[frameIndex]);
				frames.get(frame).pushAttempt(rolls[frameIndex + 1]);
				if (frame == 9) { // bonus case for last roll, to print only last roll
					frames.get(frame).pushAttempt(rolls[frameIndex + 2]);
				}
				frameIndex += 2;
			} else { // no strike or spare, sum the two rolls of frame
				score += sumTwoRolls(frameIndex);
				frames.get(frame).pushAttempt(rolls[frameIndex]);
				frames.get(frame).pushAttempt(rolls[frameIndex + 1]);
				frameIndex += 2;
			}
			frames.get(frame).setFrameScore(score);
		}

		return score;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Frame frame : frames) {
			sb.append(frame);
			sb.append("\t");
		}
		sb.append("\n");

		for (Frame frame : frames) {
			sb.append(frame.getFrameScore());
			sb.append("\t");
		}
		sb.append("\n\n");

		return sb.toString();
	}

	private int sumTwoRolls(int frameIndex) {
		return rolls[frameIndex] + rolls[frameIndex + 1];
	}

	private boolean isSpare(int frameIndex) {
		return rolls[frameIndex] + rolls[frameIndex + 1] == 10;
	}

	private int getSpareBonus(int frameIndex) {
		return rolls[frameIndex + 2];
	}

	private boolean isStrike(int frameIndex) {
		return rolls[frameIndex] == MAX_PINS;
	}

	private int getStrikeBonus(int frameIndex) {
		return rolls[frameIndex + 1] + rolls[frameIndex + 2];
	}
}
