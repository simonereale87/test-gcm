package it.simonereale.bowling.model;

import java.util.ArrayList;
import java.util.List;

public class Frame {

	private List<Integer> attempts = new ArrayList<>();
	private Integer score;
	private boolean isStrike = false;
	private boolean isSpare = false;

	public void setFrameScore(int score) {
		this.score = score;
	}

	public int getFrameScore() {
		return score;
	}

	public boolean isStrike() {
		return isStrike;
	}

	public void setStrike(boolean isStrike) {
		this.isStrike = isStrike;
	}

	public boolean isSpare() {
		return isSpare;
	}

	public void setSpare(boolean isSpare) {
		this.isSpare = isSpare;
	}

	public List<Integer> getAttempts() {
		return attempts;
	}

	public void pushAttempt(int attempt) {
		this.attempts.add(attempt);
	}

	@Override
	public String toString() {
		String toRet = "";
		if (isStrike()) {
			toRet += "[_]";
		} else if (isSpare()) {
			toRet += this.attempts.get(0) + "/";
			if (this.attempts.size() > 2) {
				toRet += this.attempts.get(2);
			}
			return toRet;
		} else {
			toRet += this.attempts;
		}
		return toRet;
	}

}
